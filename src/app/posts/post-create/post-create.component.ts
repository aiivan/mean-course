import {
  Component
  //,EventEmitter
  //,Output
} from '@angular/core';
import { NgForm } from '@angular/forms';

import { PostService } from '../post.service';

@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.css']
})
export class PostCreateComponent {
  enteredTitle = '';
  enteredContent = '';

  //@Output()
  //postCreated = new EventEmitter<Post>();

  newPost = 'alternative method of binding';

  constructor (public postsService: PostService) {}

  onAddPost2 (postInput: HTMLTextAreaElement) {
    console.log(postInput);
    //this.newPost = postInput.value;
  }

  onAddPost (form: NgForm) {
    if(form.invalid) {
      return;
    }
    //this.newPost = this.enteredValue;
    //const post: Post = {
      //title: form.value.title,
      //content: form.value.content
    //};
    //this.postCreated.emit(post);
    this.postsService.addPost(null, form.value.title, form.value.content);
    form.resetForm();
  }
}
